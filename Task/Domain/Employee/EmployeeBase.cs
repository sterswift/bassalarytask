﻿namespace Task.Domain.Employee
{
    public abstract class EmployeeBase
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public decimal Salary { get; set; }

        public EmployeeType EmployeeType { get; set; }

        public abstract void CalculateSalary();
    }
}