﻿namespace Task.Domain.Employee
{
    public enum EmployeeType
    {
        PartTime = 0,
        FullTime = 1
    }
}