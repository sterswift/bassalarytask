﻿namespace Task.Domain.Employee
{
    public class FullTimeEmployee : EmployeeBase
    {
        private readonly decimal _salary;

        public FullTimeEmployee(decimal salary)
        {
            _salary = salary;
        }

        public override void CalculateSalary()
        {
            Salary = _salary;
        }
    }
}