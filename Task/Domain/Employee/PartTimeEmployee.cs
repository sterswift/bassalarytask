﻿using System;
using System.Configuration;

namespace Task.Domain.Employee
{
    public class PartTimeEmployee : EmployeeBase
    {
        private readonly decimal _salaryPerHour;
        private readonly decimal _daysPerMonth = default (decimal);
        private readonly decimal _hoursPerDay = default (decimal);

        public PartTimeEmployee(decimal salaryPerHour)
        {
            _salaryPerHour = salaryPerHour;
            if (decimal.TryParse(ConfigurationManager.AppSettings["DaysPerMonth"], out decimal daysPerMonth))
                _daysPerMonth = daysPerMonth;

            if (decimal.TryParse(ConfigurationManager.AppSettings["HoursPerDay"], out decimal hoursPerDay))
                _hoursPerDay = hoursPerDay;

        }

        public override void CalculateSalary()
        {
            try
            {
                if (_daysPerMonth == default(decimal) || _hoursPerDay == default(decimal))
                    throw new ArgumentNullException();

                Salary = _salaryPerHour * _daysPerMonth * _hoursPerDay;
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }
    }
}