﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task.Domain.Employee;
using Task.Infrastructure.Exceptions;
using Task.Infrastructure.Providers;

namespace Task.Domain.Helpers
{
    public class FileHelper : IFileHelper
    {
        private readonly ILocalFileStorageProvider _localFileStorageProvider;

        public FileHelper(ILocalFileStorageProvider localFileStorageProvider)
        {
            _localFileStorageProvider = localFileStorageProvider;
        }

        public IEnumerable<EmployeeBase> ReadFile(string fileName)
        {
            try
            {
                var rawEmployees = _localFileStorageProvider.Read(fileName, "csv");
                var employees = new List<EmployeeBase>();

                foreach (var rawEmployee in rawEmployees)
                {
                    var employee = rawEmployee.Split(';');

                    if (!long.TryParse(employee[0], out long correctId))
                        throw new IncorrectFileFormatException();
                    var id = correctId;

                    var name = employee[1];

                    if (!decimal.TryParse(employee[2], out decimal correctSalary))
                        throw new IncorrectFileFormatException();
                    var salaryToCalculate = correctSalary;

                    if (!byte.TryParse(employee[3], out byte correctEmployeeType))
                        throw new IncorrectFileFormatException();
                    var employeeType = correctEmployeeType;

                    switch (employeeType)
                    {
                        case (byte)EmployeeType.PartTime:
                            employees.Add(new PartTimeEmployee(Convert.ToDecimal(salaryToCalculate))
                            {
                                Id = id,
                                Name = name
                            });
                            break;
                        case (byte)EmployeeType.FullTime:
                            employees.Add(new FullTimeEmployee(Convert.ToDecimal(salaryToCalculate))
                            {
                                Id = id,
                                Name = name
                            });
                            break;
                    }


                }

                return employees;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }

        public void WriteToFile(IEnumerable<EmployeeBase> employees, string fileName)
        {
            try
            {
                var rawEmployees = employees.Select(employee => $"{employee.Id};{employee.Name};{employee.Salary};{employee.EmployeeType}").ToList();

                _localFileStorageProvider.Write(fileName, "csv", rawEmployees);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }
    }
}