﻿using System.Collections.Generic;
using Task.Domain.Employee;

namespace Task.Domain.Helpers
{
    public interface IFileHelper
    {
        IEnumerable<EmployeeBase> ReadFile(string fileName);

        void WriteToFile(IEnumerable<EmployeeBase> employees, string fileName);
    }
}