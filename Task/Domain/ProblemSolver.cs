﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task.Domain.Employee;

namespace Task.Domain
{
    public class ProblemSolver
    {
        private readonly IEnumerable<EmployeeBase> _orderedEmployees;

        public ProblemSolver(IEnumerable<EmployeeBase> employees)
        {
            var employeeBases = employees as EmployeeBase[] ?? employees.ToArray();

            foreach (var employee in employeeBases)
            {
                employee.CalculateSalary();
            }

            _orderedEmployees = employeeBases.OrderByDescending(x => x.Salary).ThenBy(x => x.Name);
        }

        public void Start()
        {
            ItemA();
            ItemB();
            ItemC();
            ItemD();
            ItemE();
            
            Console.ReadKey();
        }

        private void ItemA()
        {
            Console.WriteLine("Task 1 - Employee list ordered by descending salary");
            Console.WriteLine("==========================");
            Console.WriteLine();

            Console.WriteLine("Id\tName\tSalary");
            Console.WriteLine();

            foreach (var orderedEmployee in _orderedEmployees)
            {
                Console.WriteLine($"{orderedEmployee.Id}\t{orderedEmployee.Name}\t{orderedEmployee.Salary}");
            }

            Console.WriteLine();
        }

        private void ItemB()
        {
            Console.WriteLine("Task 2 - First five employee from ordered list");
            Console.WriteLine("==========================");
            Console.WriteLine();

            Console.WriteLine("Name");
            Console.WriteLine();

            foreach (var orderedEmployeeName in _orderedEmployees.Take(5).Select(x => x.Name))
            {
                Console.WriteLine($"{orderedEmployeeName}");
            }

            Console.WriteLine();
        }

        private void ItemC()
        {
            Console.WriteLine("Task 3 - Last three identifier from ordered list");
            Console.WriteLine("==========================");
            Console.WriteLine();

            Console.WriteLine("Id");
            Console.WriteLine();

            foreach (var orderedEmployeeId in _orderedEmployees.Skip(_orderedEmployees.Count() - 3).Select(x => x.Id))
            {
                Console.WriteLine($"{orderedEmployeeId}");
            }

            Console.WriteLine();
        }

        private void ItemD()
        {
            Console.WriteLine("Task 4");
            Console.WriteLine("==========================");
            Console.WriteLine();
            Console.WriteLine("Done. IO works fine");
            Console.WriteLine();
        }

        private void ItemE()
        {
            Console.WriteLine("Task 5");
            Console.WriteLine("==========================");
            Console.WriteLine();
            Console.WriteLine("Done. File format check implemented");
            Console.WriteLine();
        }
    }
}