using System.Collections.Generic;

namespace Task.Infrastructure.Providers
{
    public interface ILocalFileStorageProvider
    {
        IEnumerable<string> Read(string fileName, string ext);
        void Write(string fileName, string ext, IEnumerable<string> lines);
    }
}