using System;
using System.Collections.Generic;
using System.IO;

namespace Task.Infrastructure.Providers
{
    public class LocalFileStorageProvider : ILocalFileStorageProvider
    {
        private readonly string _rootPath;

        public LocalFileStorageProvider(string rootPath)
        {
            _rootPath = rootPath;
        }

        private string GetPath(string fileName, string ext)
        {
            return $"{_rootPath}/{fileName}.{ext}";
        }

        public IEnumerable<string> Read(string fileName, string ext)
        {
            var lines = new List<string>();
            using (var reader = new StreamReader(GetPath(fileName, ext)))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    lines.Add(line);
                }
            }

            return lines;
        }

        public void Write(string fileName, string ext, IEnumerable<string> lines)
        {
            var path = GetPath(fileName, ext);
            var fileInfo = new FileInfo(path);

            if (fileInfo.Directory == null)
            {
                throw new InvalidOperationException("Incorrect directory path.");
            }

            using (var writer = new StreamWriter(path))
            {
                foreach (var line in lines)
                {
                    writer.WriteLine(line);
                    writer.Flush();
                }
            }
        }
        
    }
}