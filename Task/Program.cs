﻿using System.Configuration;
using Task.Domain;
using Task.Domain.Helpers;
using Task.Infrastructure.Providers;

namespace Task
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileHelper = new FileHelper(new LocalFileStorageProvider(ConfigurationManager.AppSettings["RootPath"]));
            var employees = fileHelper.ReadFile("Salaries");
            var problemSolver = new ProblemSolver(employees);
            problemSolver.Start();

        }

        
    }
}
